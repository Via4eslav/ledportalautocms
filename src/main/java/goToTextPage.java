import org.openqa.selenium.WebDriver;

/**
 * Created by ivanov.v on 18.04.2017.
 */
public class goToTextPage extends locators{

    public goToTextPage(WebDriver driver){this.driver = driver;}

    public void setContent(){driver.findElement(content).click();}

    public void setAddTextPage(){driver.findElement(addTextPage).click();}

    public void goToTxt(){
        this.setContent();
        this.setAddTextPage();
    }
}
