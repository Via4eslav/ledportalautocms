import org.openqa.selenium.WebDriver;

/**
 * Created by ivanov.v on 19.04.2017.
 */
public class go_to_Slider extends locators {

    public go_to_Slider(WebDriver driver){this.driver = driver;}

    public void setMultimedia(){driver.findElement(multimedia).click();}

    public void setAdd_Img(){driver.findElement(add_img).click();}

    public void goToSliderPage(){
        this.setMultimedia();
        this.setAdd_Img();
    }
}
