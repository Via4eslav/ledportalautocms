import org.openqa.selenium.WebDriver;

/**
 * Created by ivanov.v on 18.04.2017.
 */
public class create_text_page extends locators {

    public create_text_page(WebDriver driver){this.driver = driver;}

   // public void setPublicStatus(String strPublicStatus){driver.findElement(publicStatus).sendKeys(strPublicStatus);}

    public void setNameField(String strNameField){driver.findElement(nameField).sendKeys(strNameField);}

    public void setAlias(){driver.findElement(alias).click();}

    public void setContentTextArea()  {
        driver.findElement(contentTextArea).click();
        driver.findElement(contentTextArea).sendKeys("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do " +
                "eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco " +
                "laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu " +
                "fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia +" +
                "deserunt mollit anim id est laborum.");
    }

    public void setH_One(String strH_One){driver.findElement(hOne).sendKeys(strH_One);}

    public void setTitle(String strTitle){driver.findElement(title).sendKeys(strTitle);}

    public void setKeywords(String strKeywords){driver.findElement(keywords).sendKeys(strKeywords);}

    public void setDescription(String strDescription){driver.findElement(description).sendKeys(strDescription);}

    public void setSaveAndClose(){driver.findElement(saveAndClose).click();}

    public void enter_data_to_textPage(String strNameField, String strH_One, String strTitle, String strKeywords, String strDescription ){
        this.setNameField(strNameField);
        this.setAlias();
        this.setContentTextArea();
        this.setH_One(strH_One);
        this.setTitle(strTitle);
        this.setKeywords(strKeywords);
        this.setDescription(strDescription);
        this.setSaveAndClose();
    }
}
