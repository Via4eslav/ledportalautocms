import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by ivanov.v on 18.04.2017.
 */
public class locators {
    public WebDriver driver;

    //Login page locators

    By login = By.id("login");
    By password = By.id("password");
    By enterButton = By.id("enterLink");

    //Confirm that user has logged in

    By pageTitle = By.cssSelector(".page-title>h3");

    //Control panel locators

    By content = By.xpath(".//*[@id='mCSB_1_container']/div/ul/li[2]/a");
    By addTextPage = By.xpath(".//*[@id='mCSB_1_container']/div/ul/li[2]/ul/li[2]/a");

    //Adding text page locators

    //By publicStatus = By.xpath(".//*[@id='myForm']/div[2]/div/div[2]/div/div[1]/label[3]");
    By nameField = By.cssSelector("#f_formname");
    By alias = By.cssSelector(".btn.translitAction.otherBtn");
    By contentTextArea = By.id("f_formtext_ifr");

    //meta data locators

    By hOne = By.cssSelector("#f_formh");
    By title = By.cssSelector("#f_formtitle");
    By keywords = By.cssSelector("#f_formkeywords");
    By description = By.cssSelector("#f_formdescription");

    By saveAndClose = By.cssSelector(".btn.btn-lg.text-warning.bs-tooltip.btn-save.liTipLink");


    //Потом напишу
    By multimedia = By.xpath("html/body/div[1]/div[1]/div[1]/div[1]/div/div/ul/li[5]/a");
    By add_img = By.xpath("html/body/div[1]/div[1]/div[1]/div[1]/div/div/ul/li[5]/ul/li[2]/a");
    By addSliderButton = By.xpath("html/body/div[1]/div[2]/div/div[3]/div/div/div/div[1]/div/div[1]/div[1]");
    By sendPackImages = By.xpath("html/body/div[1]/div[2]/div/div[3]/div/div/div/div[1]/div/div[1]/div[3]/div");



}
